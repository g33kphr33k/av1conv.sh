#!/usr/bin/env bash

# Make the script panic properly when things go wrong
set -euo pipefail

#  Tweak these to your heart's content
directory="/mnt/movies/"
verbose=false
preset=8
crf=38
gop=300
size="1G"
remove_input_file=false
lazy=false
av1=false
force=false
ignore=false
force_reencode=false
resize=false 
ffmpeg_threads=10
total_space_saved=0
detect_grain=false
temp_root="/tmp"
cleanup_on_exit=true

# SVT-AV1 specific parameters
svt_tune=1
svt_enable_overlays=1
svt_scd=1
svt_fast_decode=1
svt_lookahead=120
svt_enable_qm=1
svt_qm_min=0
svt_qm_max=15
svt_tile_columns=1
svt_film_grain=0  # Default to off


# Got your own special ffmpeg? Put it here
personal_ffmpeg_path="/home/karl/.local/bin/ffmpeg"

# The tools we need to make the magic happen
dependencies=("ffprobe" "ffmpeg" "nice" "numfmt" "find" "awk" "mkvpropedit" "jq" "parallel")

# Tell us what's happening (or not)
log() {
    local message="$1"
    local colour="${2:-}"
    local force_output="${3:-false}"
    if [[ $verbose == true || $force_output == true || "$colour" == "31" ]]; then  # 31 is red
        if [[ -n "$colour" ]]; then
            printf "\033[%sm%s\033[0m\n" "$colour" "$message" >&2
        else
            printf "%s\n" "$message" >&2
        fi
    fi
    printf "%s - %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "$message" >> "/tmp/encode.log"
}

# RTFM
display_help() {
    cat << EOF
Usage: ${0##*/} [options]

Options:
  -d, --dir DIR            Specify the directory to scan (default: /mnt/movies/)
  -1, --av1                Force AV1 encoding for all files
  -f, --force              Process files without prompting
  -F, --force-reencode     Force re-encoding even if files are already in AV1 format
  -h, --help               Display this help message and exit
  -v, --verbose            Enable verbose output
  -c, --crf VALUE          Set CRF value (default: 38)
  -p, --preset VALUE       Set preset value (default: 8)
  -g, --gop VALUE          Set GOP size (default: 300)
  -i, --ignore             Ignore certain file types (e.g., CAM, WORKPRINT, TELESYNC)
  -l, --lazy               Run ffmpeg with 'nice' for lower CPU priority
  -s, --size VALUE         Minimum file size to process (e.g., 500M, 1G; default: 1G)
  -r, --remove             Remove the input file after successful encoding
  -R, --resize-1080p       Resize videos to 1080p if source resolution is higher
  --temp-dir DIR           Specify custom temporary directory (default: /tmp)
  --keep-temp              Don't clean up temporary files (for debugging)

  SVT-AV1 Specific Options:
  --svt-tune VALUE         Set tune value (default: 1)
  --svt-overlays VALUE     Enable/disable overlays (default: 1)
  --svt-scd VALUE          Enable/disable scene change detection (default: 1)
  --svt-fast-decode VALUE  Set fast decode value (default: 1)
  --svt-lookahead VALUE    Set lookahead frames (default: 120)
  --svt-enable-qm VALUE    Enable quantization matrices (default: 1)
  --svt-qm-min VALUE       Set min quantization matrix value (default: 0)
  --svt-qm-max VALUE       Set max quantization matrix value (default: 15)
  --svt-tile-columns VALUE Set number of tile columns (default: 1)

Film Grain Options:
  --svt-film-grain VALUE   Set film grain level (0-50, default: 0)
                           0: Disabled
                           1-5: Light grain (good for digital content)
                           6-10: Moderate grain (good for most film content)
                           11-20: Strong grain (for heavy film grain content)
                           21-50: Very strong grain (rarely needed)
  --detect-grain           Enable automatic film grain detection based on source analysis
                           Note: Film grain synthesis significantly impacts encoding speed

  Note:
  - All output files will be in Matroska (.mkv) format.
  - All audio is set to English as default track.
EOF
}

# Create and manage temporary directory
setup_temp_directory() {
    # Create a unique temporary directory
    temp_dir=$(mktemp -d "${temp_root}/av1conv.XXXXXXXXXX")
    if [[ ! -d "$temp_dir" ]]; then
        log "Failed to create temporary directory" "31" true
        exit 1
    fi
    log "Created temporary directory: $temp_dir" "32"
    
    # Ensure temp directory is cleaned up on script exit
    if [[ $cleanup_on_exit == true ]]; then
        trap cleanup_temp_directory EXIT
    fi
}

# Clean up temporary directory
cleanup_temp_directory() {
    if [[ -d "$temp_dir" ]]; then
        log "Cleaning up temporary directory: $temp_dir" "33"
        rm -rf "$temp_dir"
    fi
}

# Move completed file to destination
# Move completed file to destination
move_completed_file() {
    local temp_file="$1"
    local final_destination="$2"
    local destination_dir
    destination_dir=$(dirname "$final_destination")

    # Ensure destination directory exists
    if [[ ! -d "$destination_dir" ]]; then
        mkdir -p "$destination_dir"
    fi

    # Check if source file exists
    if [[ ! -f "$temp_file" ]]; then
        log "Error: Source file does not exist: $temp_file" "31" true
        return 1
    fi

    # Check if there's enough space in the destination
    local file_size
    file_size=$(stat -c %s "$temp_file") || {
        log "Error: Could not determine file size of $temp_file" "31" true
        return 1
    }

    local dest_free
    dest_free=$(df -P "$destination_dir" | awk 'NR==2 {print $4 * 1024}') || {
        log "Error: Could not determine free space in destination" "31" true
        return 1
    }

    if [[ $file_size -gt $dest_free ]]; then
        log "Error: Not enough space in destination directory" "31" true
        return 1
    fi

    # Move the file
    if ! mv "$temp_file" "$final_destination"; then
        log "Error: Failed to move file to destination" "31" true
        return 1
    fi

    log "Successfully moved file to: $final_destination" "32"
    return 0
}

# Make sure we've got all our toys in the sandbox
check_dependencies() {
    # Check for ffmpeg separately
    if [[ -x "$personal_ffmpeg_path" ]]; then
        echo "Using personal ffmpeg installation at $personal_ffmpeg_path"
        ffmpeg_path="$personal_ffmpeg_path"
    elif command -v ffmpeg &> /dev/null; then
        echo "Using system-wide ffmpeg installation"
        ffmpeg_path=$(command -v ffmpeg)
    else
        echo "Error: ffmpeg not found. Please install it and try again." >&2
        exit 1
    fi

    # Check other dependencies
    for dep in "${dependencies[@]}"; do
        if ! command -v "$dep" &> /dev/null; then
            echo "Error: $dep is not installed. Please install it and try again." >&2
            exit 1
        fi
    done
}

# Peek inside the video file
get_video_info() {
    local file="$1"
    local info
    
    # Redirect stderr to stdout and capture both
    if ! info=$(ffprobe -v error -select_streams v:0 \
        -show_entries stream=codec_name,width,height \
        -of default=noprint_wrappers=1:nokey=1 "$file" 2>&1); then
        # Only return the error indicator, don't log here
        echo "ERROR:CORRUPT_FILE"
        return 1
    fi

    # Check if we got valid output
    if [[ -z "$info" ]]; then
        echo "ERROR:NO_VIDEO"
        return 1
    fi

    echo "$info"
    return 0
}

# Check subtitle formats and return appropriate ffmpeg flags
check_subtitle_formats() {
    local file="$1"
    local info
    info=$(ffprobe -v error -select_streams s \
        -show_entries stream=index,codec_name:stream_tags=language \
        -of json "$file" 2>/dev/null)
    
    # If no subtitles found or empty info
    if [[ -z "$info" || "$info" == "{}" ]]; then
        echo ""
        return
    fi

    # Initialize arrays for different subtitle types
    local eng_text_subs=()
    local eng_pgs_subs=()

    # Process each subtitle stream
    while IFS= read -r stream; do
        local index lang codec
        index=$(echo "$stream" | jq -r '.index')
        lang=$(echo "$stream" | jq -r '.tags.language // "und"')
        codec=$(echo "$stream" | jq -r '.codec_name // ""')
        
        if [[ "$lang" == "eng" ]]; then
            if [[ "$codec" == "subrip" || "$codec" == "mov_text" ]]; then
                eng_text_subs+=("$index")
            elif [[ "$codec" == "hdmv_pgs_subtitle" || "$codec" == "pgssub" ]]; then
                eng_pgs_subs+=("$index")
            fi
        fi
    done < <(echo "$info" | jq -c '.streams[]')

    # Build mapping options
    local map_options=()

    # First try to use text-based subtitles
    for idx in "${eng_text_subs[@]:0:2}"; do
        map_options+=("-map" "0:$idx")
    done

    # If we have no text subs but have PGS subs, use those
    if [[ ${#map_options[@]} -eq 0 && ${#eng_pgs_subs[@]} -gt 0 ]]; then
        for idx in "${eng_pgs_subs[@]:0:2}"; do
            map_options+=("-map" "0:$idx")
        done
    fi

    # If we found any subtitles, add the appropriate codec
    if [[ ${#map_options[@]} -gt 0 ]]; then
        # If we're using text subs, convert to SRT
        if [[ ${#eng_text_subs[@]} -gt 0 ]]; then
            map_options+=("-c:s" "srt")
            log "Including text-based English subtitles: ${map_options[*]}" "32"
        else
            # For PGS subs, just copy them
            map_options+=("-c:s" "copy")
            log "Including PGS English subtitles: ${map_options[*]}" "32"
        fi
    fi

    # Return the mapping options
    echo "${map_options[*]}"
}

# Detect if film grain should be used and set sane values
detect_film_grain() {
    local file="$1"
    local sample_duration=10
    
    log "Analyzing source material for grain profile..." "33" true

    # Clear the temp file first
    local temp_file="/tmp/noise_analysis.txt"
    > "$temp_file"

    # Run ffmpeg with reduced output
    ffmpeg -hide_banner -i "$file" -t "$sample_duration" \
        -filter_complex "[0:v]split=2[ref][dist];[dist]framerate=fps=24000/1001[dist1];[ref][dist1]psnr=stats_file=-" \
        -f null - 2>&1 | grep "PSNR" > "$temp_file"

    # Extract detailed PSNR values
    local psnr_line=$(grep 'PSNR.*average' "$temp_file")
    local psnr_avg=$(echo "$psnr_line" | grep -oP 'average:\K[0-9.]+')
    local psnr_y=$(echo "$psnr_line" | grep -oP 'y:\K[0-9.]+')
    local psnr_min=$(echo "$psnr_line" | grep -oP 'min:\K[0-9.]+')

    # Full details to log only
    log "Analysis Details:" "33"
    log "Average PSNR: $psnr_avg" "33"
    log "Luma PSNR (Y): $psnr_y" "33"
    log "Minimum PSNR: $psnr_min" "33"

    # Determine source type and grain settings
    local source_type=""
    if (( $(echo "$psnr_avg > 55" | bc -l) )); then
        source_type="Very clean digital source (modern)"
        svt_film_grain=0
    elif (( $(echo "$psnr_avg > 45" | bc -l) )); then
        source_type="Clean digital source"
        svt_film_grain=0 
    elif (( $(echo "$psnr_avg > 35" | bc -l) )); then
        source_type="Good quality source with natural noise"
        svt_film_grain=3 
    elif (( $(echo "$psnr_avg > 30" | bc -l) )); then
        source_type="Source contains noticeable grain"
        svt_film_grain=5
    else
        source_type="High grain/noise content"
        svt_film_grain=8
    fi

    # Console output - brief and clear
    echo "Source Analysis:"
    echo "→ Content type: $source_type"
    echo "→ Setting grain strength to: $svt_film_grain"
    echo

    return 0
}

# Track how much space we're saving
update_space_saved() {
    local original_size="$1"
    local new_size="$2"
    
    # Convert sizes to bytes
    local original_bytes=$(numfmt --from=iec "$original_size")
    local new_bytes=$(numfmt --from=iec "$new_size")
    
    # Calculate space saved in bytes
    local saved_bytes=$((original_bytes - new_bytes))
    
    # Update total space saved
    total_space_saved=$((total_space_saved + saved_bytes))
    
    # Convert saved to human-readable format
    local saved_human=$(numfmt --to=iec --format="%.2f" "$saved_bytes")
    
    echo "$saved_human"
}

# Process Audio Channels
process_audio() {
    local file="$1"
    local -n audio_options_ref="$2"  # Create a nameref to the variable provided
    local audio_streams
    audio_streams=$(ffprobe -v error -select_streams a \
        -show_entries stream=index:stream_tags=language:stream=channels \
        -of csv=p=0 "$file" 2>/dev/null)

    audio_options_ref=()  # Initialize the referenced array
    local output_audio_index=0  # Initialize output audio stream index

    local IFS=$'\n'  # Localize IFS to this function
    for stream in $audio_streams; do
        IFS=',' read -r index channels language <<< "$stream"
        audio_options_ref+=("-map" "0:$index")

        local num_channels=${channels:-2}  # Default to 2 if channels is empty

        case $num_channels in
            1) audio_options_ref+=("-c:a:$output_audio_index" "libopus" \
                "-b:a:$output_audio_index" "64k" "-ac:a:$output_audio_index" "1") ;;
            2) audio_options_ref+=("-c:a:$output_audio_index" "libopus" \
                "-b:a:$output_audio_index" "128k" "-ac:a:$output_audio_index" "2") ;;
            6) audio_options_ref+=("-c:a:$output_audio_index" "libopus" \
                "-b:a:$output_audio_index" "384k" "-ac:a:$output_audio_index" "6") ;;
            8) audio_options_ref+=("-c:a:$output_audio_index" "libopus" \
                "-b:a:$output_audio_index" "512k" "-ac:a:$output_audio_index" "8") ;;
            *) audio_options_ref+=("-c:a:$output_audio_index" "libopus" \
                "-b:a:$output_audio_index" "128k" "-ac:a:$output_audio_index" "$num_channels") ;;
        esac

        if [[ -n "$language" && "$language" != "und" ]]; then
            audio_options_ref+=("-metadata:s:a:$output_audio_index" "language=$language")
        fi

        output_audio_index=$((output_audio_index + 1))  # Increment output index
    done

    log "Audio streams: $output_audio_index" "33"

    # Join array elements for logging
    local audio_options_formatted
    audio_options_formatted=$(printf '%s ' "${audio_options_ref[@]}")

    log "Audio encoding options: $audio_options_formatted" "33"
}

# Give the file a proper name, no more codec alphabet soup
transform_filename() {
    local filename="$1"
    local directory
    directory=$(dirname "$filename")
    local basefilename
    basefilename=$(basename "$filename")
    
    # Remove the extension from the base filename
    local basefilename_no_ext="${basefilename%.*}"
    
    # Clean up the filename, including resolution references if resize=true
    local cleanfilename
    if [[ $resize == true ]]; then
        cleanfilename=$(echo "$basefilename_no_ext" | sed -E '
            s/\b(x264|x265|h264|h265|hevc|xvid|divx|mpeg2|mpeg4|vp9|avc)\b//Ig;
            s/\b(264|265)\b//Ig;
            s/\b(4k|2160p|2k|1440p|uhd)\b/1080p/Ig;
            s/[[:space:]]*,[[:space:]]*/,/g;
            s/,+/,/g;
            s/^[[:space:],.]+//;
            s/[[:space:],.]+$//;
            s/\.{2,}/./g;
        ')
    else
        cleanfilename=$(echo "$basefilename_no_ext" | sed -E '
            s/\b(x264|x265|h264|h265|hevc|xvid|divx|mpeg2|mpeg4|vp9|avc)\b//Ig;
            s/\b(264|265)\b//Ig;
            s/[[:space:]]*,[[:space:]]*/,/g;
            s/,+/,/g;
            s/^[[:space:],.]+//;
            s/[[:space:],.]+$//;
            s/\.{2,}/./g;
        ')
    fi
    
    # Append ", AV1.mkv" to the cleaned filename
    if [[ "$cleanfilename" =~ ,$ ]]; then
        cleanfilename="${cleanfilename}, AV1.mkv"
    elif [[ "$cleanfilename" =~ \)$ ]]; then
        cleanfilename="${cleanfilename%)} AV1).mkv"
    else
        cleanfilename+=", AV1.mkv"
    fi
    
    echo "$directory/$cleanfilename"
}

# Parse Command Line Arguments
parse_arguments() {
    while [[ $# -gt 0 ]]; do
        case "$1" in
            -d|--dir) directory="$2"; shift 2 ;;
            -1|--av1) av1=true; shift ;;
            -f|--force) force=true; shift ;;
            -F|--force-reencode) force_reencode=true; shift ;;
            -h|--help) display_help; exit 0 ;;
            -v|--verbose) verbose=true; shift ;;
            -c|--crf) crf="$2"; shift 2 ;;
            -p|--preset) preset="$2"; shift 2 ;;
            -g|--gop) gop="$2"; shift 2 ;;
            -i|--ignore) ignore=true; shift ;;
            -l|--lazy) lazy=true; shift ;;
            -s|--size) size="$2"; shift 2 ;;
            -r|--remove) remove_input_file=true; shift ;;
            -R|--resize-1080p) resize=true; shift ;; 
            --svt-tune) svt_tune="$2"; shift 2 ;;
            --svt-overlays) svt_enable_overlays="$2"; shift 2 ;;
            --svt-scd) svt_scd="$2"; shift 2 ;;
            --svt-fast-decode) svt_fast_decode="$2"; shift 2 ;;
            --svt-lookahead) svt_lookahead="$2"; shift 2 ;;
            --svt-enable-qm) svt_enable_qm="$2"; shift 2 ;;
            --svt-qm-min) svt_qm_min="$2"; shift 2 ;;
            --svt-qm-max) svt_qm_max="$2"; shift 2 ;;
            --svt-tile-columns) svt_tile_columns="$2"; shift 2 ;;
            --svt-film-grain) svt_film_grain="$2"; shift 2 ;;
            --detect-grain) detect_grain=true; shift ;;
            --temp-dir) temp_root="$2"; shift 2 ;;
            --keep-temp) cleanup_on_exit=false; shift ;;
            --) shift; break ;;
            -*) echo "Unknown option: $1" >&2; display_help; exit 1 ;;
            *) break ;;
        esac
    done
}

# Validate Arguments
validate_arguments() {
    # Existing size validation
    if [[ ! $size =~ ^[0-9]+[KMGTP]?$ ]]; then
        echo "Error: Size must be a positive number optionally followed by K, M, G, T, or P" >&2
        exit 1
    fi

    if [[ ! $size =~ [KMGTP]$ ]]; then
        size="${size}G"
    fi

    # Existing encoder parameter validation
    if ! [[ $crf =~ ^[0-9]+$ ]] || [[ $crf -lt 0 || $crf -gt 63 ]]; then
        echo "Error: CRF must be an integer between 0 and 63" >&2
        exit 1
    fi
    if ! [[ $preset =~ ^[0-9]+$ ]] || [[ $preset -lt 0 || $preset -gt 12 ]]; then
        echo "Error: Preset must be an integer between 0 and 12" >&2
        exit 1
    fi
    if ! [[ $gop =~ ^[0-9]+$ ]] || [[ $gop -lt 0 || $gop -gt 500 ]]; then
        echo "Error: GOP must be an integer between 0 and 500" >&2
        exit 1
    fi

    # SVT-AV1 parameter validation
    if ! [[ $svt_tune =~ ^[0-1]$ ]]; then
        echo "Error: SVT tune must be either 0 (PSNR) or 1 (VMAF)" >&2
        exit 1
    fi

    if ! [[ $svt_enable_overlays =~ ^[0-1]$ ]]; then
        echo "Error: SVT enable-overlays must be either 0 or 1" >&2
        exit 1
    fi

    if ! [[ $svt_scd =~ ^[0-1]$ ]]; then
        echo "Error: SVT scene change detection (scd) must be either 0 or 1" >&2
        exit 1
    fi

    if ! [[ $svt_fast_decode =~ ^[0-2]$ ]]; then
        echo "Error: SVT fast-decode must be 0, 1, or 2" >&2
        exit 1
    fi

    if ! [[ $svt_lookahead =~ ^[0-9]+$ ]] || [[ $svt_lookahead -lt 0 || $svt_lookahead -gt 240 ]]; then
        echo "Error: SVT lookahead must be an integer between 0 and 240" >&2
        exit 1
    fi

    if ! [[ $svt_enable_qm =~ ^[0-1]$ ]]; then
        echo "Error: SVT enable-qm must be either 0 or 1" >&2
        exit 1
    fi

    if ! [[ $svt_qm_min =~ ^[0-9]+$ ]] || [[ $svt_qm_min -lt 0 || $svt_qm_min -gt 15 ]]; then
        echo "Error: SVT qm-min must be an integer between 0 and 15" >&2
        exit 1
    fi

    if ! [[ $svt_qm_max =~ ^[0-9]+$ ]] || [[ $svt_qm_max -lt 0 || $svt_qm_max -gt 15 ]]; then
        echo "Error: SVT qm-max must be an integer between 0 and 15" >&2
        exit 1
    fi

    # Validate qm-min is not greater than qm-max
    if [[ $svt_qm_min -gt $svt_qm_max ]]; then
        echo "Error: SVT qm-min cannot be greater than qm-max" >&2
        exit 1
    fi

    if ! [[ $svt_tile_columns =~ ^[0-9]+$ ]] || [[ $svt_tile_columns -lt 0 || $svt_tile_columns -gt 4 ]]; then
        echo "Error: SVT tile-columns must be an integer between 0 and 4" >&2
        exit 1
    fi
}

# How much processor are we eating
run_ffmpeg() {
    if [[ $lazy == true ]]; then
        nice -n 19 "$ffmpeg_path" "$@"
    else
        "$ffmpeg_path" "$@"
    fi
}

# How big is this thing anyway?
get_file_size() {
    stat -c %s "$1" | numfmt --to=iec
}

# Find all the videos
generate_video_list() {
    log "Initiating file system scan..." "34"
    log "Searching in directory: $directory" "34"
    log "Minimum file size: $size" "34"

    videolist=()
    file_count=0
    found_files=()
    found_any_files=false
    
    # First count total files
    local total_files
    total_files=$(find "$directory" -type f -size "+$size" \
        \( -iname "*.mp4" -o -iname "*.mkv" -o -iname "*.avi" -o -iname "*.webm" \
           -o -iname "*.mpg" -o -iname "*.mpv" -o -iname "*.m2ts" -o -iname "*.ts" \) \
        ! -path "*/extras/*" ! -path "*/samples/*" -printf '.' | wc -c)

    # Show initial progress
    display_find_progress "$total_files" 0

    # Now collect files with progress
    while IFS= read -r -d '' file; do
        found_any_files=true
        videolist+=("$file")
        found_files+=("$file")
        file_count=$((file_count + 1))
        
        # Update progress
        display_find_progress "$total_files" "$file_count" "$file"
        
    done < <(find "$directory" -type f -size "+$size" \
        \( -iname "*.mp4" -o -iname "*.mkv" -o -iname "*.avi" -o -iname "*.webm" \
           -o -iname "*.mpg" -o -iname "*.mpv" -o -iname "*.m2ts" -o -iname "*.ts" \) \
        ! -path "*/extras/*" ! -path "*/samples/*" -print0)

    echo  # Add newline after progress display

    if ! $found_any_files; then
        echo "No files found in $directory matching the criteria."
        exit 0
    else
        log "Found ${#videolist[@]} files eligible for conversion." "32"
        
        # Display the list of found files
        echo "Found files:"
        printf '%s\n' "${found_files[@]}" | sed 's:^.*/::' | sort  # Show sorted filenames
    fi

    if [[ $ignore == true ]]; then
        log "Applying ignore filters for CAM, WORKPRINT, and TELESYNC files..." "33"
        filtered_videolist=()
        for file in "${videolist[@]}"; do
            basename=$(basename "$file")
            if [[ $basename =~ CAM|WORKPRINT|TELESYNC ]]; then
                log "Ignoring file due to filter: $file" "31"
            else
                filtered_videolist+=("$file")
            fi
        done
        videolist=("${filtered_videolist[@]}")
        log "After applying ignore filters, ${#videolist[@]} files remain." "32"
    fi

    if [[ ${#videolist[@]} -eq 0 ]]; then
        echo "No files remain after applying ignore filters."
        exit 0
    fi
}

# Should Convert Function for Parallel Processing
should_convert() {
    local file="$1"
    local info
    info=$(get_video_info "$file")
    local codec_name width height
    read -r codec_name width height <<< "$info"

    # Convert codec_name to uppercase for consistency
    local format
    format=$(echo "$codec_name" | tr '[:lower:]' '[:upper:]')

    # Decision logic
    if [[ "$force_reencode" == true ]]; then
        echo "$file"
        return
    fi

    if [[ "$av1" == true ]]; then
        if [[ "$format" != "AV1" ]]; then
            echo "$file"
        elif [[ "$verbose" == true ]]; then
            log "Skipping already AV1 encoded file: $file" "33"
        fi
    else
        if [[ "$format" != "HEVC" && "$format" != "AV1" ]]; then
            echo "$file"
        elif [[ "$verbose" == true ]]; then
            log "Skipping file with format $format: $file" "33"
        fi
    fi
}

# Export necessary functions and variables for GNU Parallel
export -f should_convert
export -f get_video_info
export av1
export force_reencode
export verbose
export -f log

# Filter videos at speed
filter_video_list() {
    log "Filtering video list based on format and encoding options..." "34"

    # Use GNU Parallel to filter files
    # The `should_convert` function echoes the filename if it needs to be converted
    # Collect all echoed filenames into `filtered_videolist`
    mapfile -t filtered_videolist < <(printf '%s\0' "${videolist[@]}" | \
        parallel -0 -j "$(nproc)" should_convert {})

    if [[ ${#filtered_videolist[@]} -eq 0 ]]; then
        log "No files to convert after filtering based on format." "31"
        return 1
    fi

    # Output the filtered list
    printf '%s\n' "${filtered_videolist[@]}"
}

# Make English Great Again (MEGA)
set_default_audio_track() {
    local file="$1"

    # Get track information in JSON format
    local json_output
    json_output=$(mkvmerge --identification-format json --identify "$file")

    # Extract audio tracks with their languages
    local audio_languages=()
    audio_languages=($(echo "$json_output" | jq -r '
        [.tracks[] | select(.type == "audio") | .properties.language // "und"] | .[]
    '))

    # Initialize variables
    local eng_track_seq=""
    local und_track_seq=""
    local first_track_seq=""
    local seq_number=1

    # Iterate over the audio tracks to find matching languages
    for lang in "${audio_languages[@]}"; do
        if [[ -z "$first_track_seq" ]]; then
            first_track_seq="$seq_number"
        fi

        if [[ "$lang" == "eng" ]]; then
            eng_track_seq="$seq_number"
            break  # Prefer English track; exit the loop
        elif [[ "$lang" == "und" ]] && [[ -z "$und_track_seq" ]]; then
            und_track_seq="$seq_number"
        fi

        seq_number=$((seq_number + 1))
    done

    # Determine which track sequence number to set as default
    local default_track_seq=""
    if [[ -n "$eng_track_seq" ]]; then
        default_track_seq="$eng_track_seq"
    elif [[ -n "$und_track_seq" ]]; then
        default_track_seq="$und_track_seq"
    else
        default_track_seq="$first_track_seq"
    fi

    if [[ -n "$default_track_seq" ]]; then
        # Build mkvpropedit command to reset default flags
        mkvpropedit_cmd=(mkvpropedit "$file")

        # Reset default flags for all audio tracks
        local reset_seq_number=1
        for _ in "${audio_languages[@]}"; do
            mkvpropedit_cmd+=(--edit track:a"$reset_seq_number" --set flag-default=0)
            reset_seq_number=$((reset_seq_number + 1))
        done

        # Set the default flag for the selected audio track
        mkvpropedit_cmd+=(--edit track:a"$default_track_seq" --set flag-default=1)

        # Execute the mkvpropedit command
        if "${mkvpropedit_cmd[@]}"; then
            log "Set default audio track to track:a$default_track_seq in $file" "32" true
        else
            log "Failed to set default audio track in $file" "31" true
        fi
    else
        log "No audio tracks found in $file to set as default" "31" true
    fi
}

encode_single_file() {
    local file="$1"
    local presize
    presize=$(get_file_size "$file")

    # Get the base filename for temp file
    local temp_filename
    temp_filename="${temp_dir}/$(basename "$(transform_filename "$file")")"
    
    # Get the final destination filename
    local final_destination
    final_destination=$(transform_filename "$file")

    log "Encoding to temporary file: $temp_filename" "32" true

    # Run grain detection if enabled and not manually set
    if [[ $detect_grain == true && $svt_film_grain -eq 0 ]]; then
        detect_film_grain "$file"
    fi

    # Spit a warning if we've enabled film grain.
    if [[ $svt_film_grain -gt 0 ]]; then
        log "WARNING: Film grain synthesis is enabled (strength: $svt_film_grain). This will significantly increase encoding time." "31" true
        echo
    fi

    # Get video codec name, width, and height
    local preformat_info
    preformat_info=$(get_video_info "$file")
    read -r codec_name width height <<< "$preformat_info"
    
    # Use codec_name as format
    local preformat
    preformat=$(echo "$codec_name" | tr '[:lower:]' '[:upper:]')
    
    log "Converting: $file (Size: $presize, Format: $preformat)" "32" true

    # Initialize audio_options as an array and pass its name to process_audio
    local audio_options=()
    process_audio "$file" audio_options

    log "Starting encode to temp file: $temp_filename" "32" true

    # Get subtitle options
    local subs_options
    read -ra subs_options <<< "$(check_subtitle_formats "$file")"

    # Get video resolution
    read -r width height <<< "$(ffprobe -v error -select_streams v:0 \
    -show_entries stream=width,height \
    -of csv=p=0:s=' ' "$file" 2>/dev/null)"

    if [[ "$resize" == true && "$height" -gt 1080 ]]; then
        scale_filter=("-vf" "scale=-2:1080")
        log "Scaling video down to 1080p" "33" true
    else
        scale_filter=()
    fi

    # Reset IFS to default before building cmd array
    IFS=$' \t\n'

    # Build the ffmpeg command in an array
    cmd=()

    # Add 'nice' if lazy is true
    if [[ $lazy == true ]]; then
        cmd+=(nice -n 19)
    fi

    # Add ffmpeg path and initial options with conditional logging level
    if [[ $verbose == true ]]; then
        cmd+=("$ffmpeg_path" -hide_banner -loglevel info -stats)
    else
        # Use warning level to show progress but hide most messages
        cmd+=("$ffmpeg_path" -hide_banner -loglevel warning -stats)
    fi

    # Add -y and input file
    cmd+=(-y -i "$file")

    # Map video stream
    cmd+=(-map 0:v:0)

    # Add scale filter if any
    cmd+=("${scale_filter[@]}")

    # Map audio streams and options
    cmd+=("${audio_options[@]}")

    # Add subtitle options
    cmd+=("${subs_options[@]}")

    # Set codecs for all streams & include subtitles
    cmd+=(-c:v libsvtav1)
    cmd+=(-svtav1-params \
    "tune=${svt_tune}:\
    enable-overlays=${svt_enable_overlays}:\
    scd=${svt_scd}:\
    fast-decode=${svt_fast_decode}:\
    keyint=$((gop * 2)):\
    lookahead=${svt_lookahead}:\
    enable-qm=${svt_enable_qm}:\
    qm-min=${svt_qm_min}:\
    qm-max=${svt_qm_max}:\
    tile-columns=${svt_tile_columns}:\
    film-grain=${svt_film_grain}")
    cmd+=(-preset "$preset" -crf "$crf" -pix_fmt yuv420p10le -g "$gop")
    cmd+=(-threads $ffmpeg_threads)

    # Output to temp file
    cmd+=("$temp_filename")

    # Log the ffmpeg command
    log "Running ffmpeg command: $(printf '%q ' "${cmd[@]}")" "32" true

    # Run the ffmpeg command
    if "${cmd[@]}"; then
        # Verify the temp file exists and has size
        if [[ ! -f "$temp_filename" || ! -s "$temp_filename" ]]; then
            log "Error: Encoding completed but temp file is missing or empty" "31" true
            return 1
        fi

        # Set default audio track to English
        set_default_audio_track "$temp_filename"

        # Move the file to its final destination
        if ! move_completed_file "$temp_filename" "$final_destination"; then
            log "Failed to move encoded file to final destination" "31" true
            return 1
        fi

        # Update MKV statistics and metadata
        update_mkv_statistics "$file" "$final_destination"

        # This will remove the source file if requested
        if [[ $remove_input_file == true ]]; then
            rm -f "$file"
            log "Source file $file removed." "31" true
        fi

        local new_size
        new_size=$(get_file_size "$final_destination")
        
        # Display space saved information
        display_space_saved "$presize" "$new_size" "$final_destination"

        # Get new format
        local new_format_info
        new_format_info=$(get_video_info "$final_destination")
        read -r new_codec_name new_width new_height <<< "$new_format_info"
        local new_format
        new_format=$(echo "$new_codec_name" | tr '[:lower:]' '[:upper:]')
        
        log "New format: $new_format, previous format: $preformat" "32" true
        echo  # Add blank line for readability
        return 0
    else
        log "Encoding failed for $file" "31" true
        # Clean up failed temp file if it exists
        [[ -f "$temp_filename" ]] && rm -f "$temp_filename"
        return 1
    fi
}

# Display Progress During File Finding
display_find_progress() {
    local total=$1
    local current=$2
    local filename=${3:-}  # Make the filename parameter optional
    local remaining=$((total - current))
    
    # If no filename is provided, just show the counter
    if [[ -z "$filename" ]]; then
        printf "\rScanning: [%d/%d]" "$current" "$total"
    else
        # Truncate the filename if it's too long
        local shortname
        shortname=$(basename "$filename")
        if [[ ${#shortname} -gt 50 ]]; then
            shortname="${shortname:0:47}..."
        fi
        printf "\rScanning: [%d/%d] %s" "$current" "$total" "$shortname"
    fi
}

# Display Encoding Progress
display_encode_progress() {
    local total=$1
    local current=$2
    local remaining=$((total - current))
    local total_saved_human=$(numfmt --to=iec --format="%.2f" "$total_space_saved")
    printf "\rProgress: [%d/%d] | Space Saved: %s" \
        "$current" "$total" "$total_saved_human"
}

# Display Space Saved
display_space_saved() {
    local presize="$1"
    local new_size="$2"
    local filename="$3"
    
    # Calculate space saved for this file
    local saved_bytes=$(($(numfmt --from=iec "$presize") - $(numfmt --from=iec "$new_size")))
    local saved_human=$(numfmt --to=iec --format="%.2f" "$saved_bytes")
    
    # Update total space saved
    total_space_saved=$((total_space_saved + saved_bytes))
    
    printf "File: %s\n" "$filename"
    printf "Original Size: %s → New Size: %s\n" "$presize" "$new_size"
    printf "Space Saved: %s\n" "$saved_human"
}

#### TABLE CREATION FUNCTIONS FOR NICE STATS ####

# A horizontal rule
hr() {
    printf '%*s\n' "${1:-$(tput cols)}" '' | tr ' ' '-'
}

# Format table row
format_row() {
    printf "| %-20s | %-20s | %-20s |\n" "$1" "$2" "$3"
}

# A section header
format_section() {
    printf "| %-64s |\n" "$1"
}

# Calculate percentages - avoid divide by zero
calculate_percentage() {
    local old="$1"
    local new="$2"
    
    # Check if either value is empty or zero
    if [[ -z "$old" || "$old" == "null" || "$old" == "0" || -z "$new" || "$new" == "null" ]]; then
        echo "N/A"
    else
        awk "BEGIN {printf \"%.2f\", (($old-$new)/$old)*100}"
    fi
}

# Format bitrate
format_bitrate() {
    local bitrate="$1"
    if [[ -z "$bitrate" || "$bitrate" == "null" || "$bitrate" == "0" ]]; then
        echo "N/A"
    else
        numfmt --to=iec --format='%.2f' "$bitrate"
    fi
}

# Function to update MKV statistics and metadata
update_mkv_statistics() {
    local input_file="$1"
    local output_file="$2"
    
    # Calculate bitrate from file size and duration
    calculate_bitrate() {
        local file_size="$1"    # in bytes
        local duration="$2"     # in seconds (might be floating point)
        
        if [[ -z "$duration" || "$duration" == "null" || "$duration" == "0" ]]; then
            echo "0"
            return
        fi
        
        # Use bc for all floating point arithmetic
        echo "scale=2; ($file_size * 8) / ($duration * 1024)" | bc -l
    }

    # Get video statistics
    local input_stats output_stats
    input_stats=$(ffprobe -v error -select_streams v:0 \
        -show_entries stream=codec_name,width,height,r_frame_rate \
        -of json "$input_file" 2>/dev/null)
    output_stats=$(ffprobe -v error -select_streams v:0 \
        -show_entries stream=codec_name,width,height,r_frame_rate \
        -of json "$output_file" 2>/dev/null)
    
    # Extract basic info
    local input_codec output_codec input_framerate output_framerate
    input_codec=$(echo "$input_stats" | jq -r '.streams[0].codec_name // "unknown"')
    output_codec=$(echo "$output_stats" | jq -r '.streams[0].codec_name // "unknown"')
    input_framerate=$(echo "$input_stats" | jq -r '.streams[0].r_frame_rate // "unknown"')
    output_framerate=$(echo "$output_stats" | jq -r '.streams[0].r_frame_rate // "unknown"')

    # Get durations from container level and ensure they're valid numbers
    local input_duration output_duration
    input_duration=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "$input_file" | sed 's/N\/A/0/')
    output_duration=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "$output_file" | sed 's/N\/A/0/')

    # Validate durations are numbers (can be floating point)
    if ! echo "$input_duration" | grep -qE '^[0-9]+\.?[0-9]*$'; then
        input_duration=0
    fi
    if ! echo "$output_duration" | grep -qE '^[0-9]+\.?[0-9]*$'; then
        output_duration=0
    fi

    # Get file sizes and calculate bitrates
    local input_size output_size
    input_size=$(stat -c%s "$input_file")
    output_size=$(stat -c%s "$output_file")
    
    # Calculate bitrates in Kbps
    local input_bitrate output_bitrate
    input_bitrate=$(calculate_bitrate "$input_size" "$input_duration")
    output_bitrate=$(calculate_bitrate "$output_size" "$output_duration")
    
    # Format for display (convert to Mbps for readability)
    local formatted_input_bitrate formatted_output_bitrate
    formatted_input_bitrate=$(echo "scale=2; $input_bitrate / 1024" | bc)
    formatted_output_bitrate=$(echo "scale=2; $output_bitrate / 1024" | bc)
    
    # Calculate reductions
    filesize_reduction=$(calculate_percentage "$input_size" "$output_size")
    bitrate_reduction=$(calculate_percentage "$input_bitrate" "$output_bitrate")
        
    # Create a temporary XML file for the tags
    local tags_file=$(mktemp)
    cat > "$tags_file" << EOF
<?xml version="1.0"?>
<Tags>
  <Tag>
    <Targets>
      <TargetTypeValue>50</TargetTypeValue>
    </Targets>
    <Simple>
      <Name>SOURCE_CODEC</Name>
      <String>$input_codec</String>
    </Simple>
    <Simple>
      <Name>TARGET_CODEC</Name>
      <String>$output_codec</String>
    </Simple>
    <Simple>
      <Name>ENCODE_DATE</Name>
      <String>$(date '+%Y-%m-%d %H:%M:%S')</String>
    </Simple>
    <Simple>
      <Name>SOURCE_FILESIZE</Name>
      <String>$(numfmt --to=iec --format='%.2f' "$input_size")</String>
    </Simple>
    <Simple>
      <Name>TARGET_FILESIZE</Name>
      <String>$(numfmt --to=iec --format='%.2f' "$output_size")</String>
    </Simple>
    <Simple>
      <Name>SIZE_REDUCTION</Name>
      <String>${filesize_reduction}%</String>
    </Simple>
    <Simple>
      <Name>SOURCE_BITRATE</Name>
      <String>${formatted_input_bitrate} Mbps</String>
    </Simple>
    <Simple>
      <Name>TARGET_BITRATE</Name>
      <String>${formatted_output_bitrate} Mbps</String>
    </Simple>
    <Simple>
      <Name>SOURCE_FRAMERATE</Name>
      <String>$input_framerate</String>
    </Simple>
    <Simple>
      <Name>TARGET_FRAMERATE</Name>
      <String>$output_framerate</String>
    </Simple>
    <Simple>
      <Name>ENCODE_SETTINGS</Name>
      <String>preset:$preset,crf:$crf,grain:$svt_film_grain</String>
    </Simple>
  </Tag>
</Tags>
EOF

    # Build the mkvpropedit command with proper XML tags
    local cmd=(mkvpropedit "$output_file"
        --edit info --set "title=${output_file%.*}"
        --tags global:"$tags_file")
    
    # Execute the command
    if "${cmd[@]}"; then
        rm -f "$tags_file"  # Clean up temporary file
        log "Updated MKV metadata and statistics for $output_file" "32" true
        
        # Display statistics table
        echo
        hr 68
        format_section "Encoding Statistics for $(basename "$output_file")"
        hr 68
        format_row "Metric" "Original" "New"
        hr 68
        format_row "Codec" "$input_codec" "$output_codec"
        format_row "File Size" "$(numfmt --to=iec --format='%.2f' "$input_size")" "$(numfmt --to=iec --format='%.2f' "$output_size")"
        format_row "Bitrate" "${formatted_input_bitrate} Mbps" "${formatted_output_bitrate} Mbps"
        format_row "Frame Rate" "$input_framerate" "$output_framerate"
        if [[ "$input_duration" != "0" && "$output_duration" != "0" ]]; then
            format_row "Duration" "$(printf "%.2f" "$input_duration")s" "$(printf "%.2f" "$output_duration")s"
        fi
        hr 68
        format_section "Reduction Statistics"
        hr 68
        format_row "Size Reduction" "${filesize_reduction}%" ""
        hr 68
        format_section "Encoding Parameters"
        hr 68
        format_row "Preset" "$preset" ""
        format_row "CRF" "$crf" ""
        format_row "Film Grain" "$svt_film_grain" ""
        format_row "GOP Size" "$gop" ""
        hr 68
        echo
        
        # Log the statistics to the log file
        log "Detailed Statistics:" "32" true
        log "Original Codec: $input_codec → New Codec: $output_codec" "32"
        log "Original Size: $(numfmt --to=iec --format='%.2f' "$input_size") → New Size: $(numfmt --to=iec --format='%.2f' "$output_size")" "32"
        log "Original Bitrate: ${formatted_input_bitrate} Mbps → New Bitrate: ${formatted_output_bitrate} Mbps" "32"
        log "Size Reduction: ${filesize_reduction}%" "32"
        log "Bitrate Reduction: ${bitrate_reduction}%" "32"
        log "Frame Rate: $input_framerate → $output_framerate" "32"
        [[ "$input_duration" != "0" && "$output_duration" != "0" ]] && \
            log "Duration: ${input_duration}s → ${output_duration}s" "32"
        log "Encoding Parameters: preset=$preset, crf=$crf, grain=$svt_film_grain, gop=$gop" "32"
    else
        rm -f "$tags_file"  # Clean up temporary file even on failure
        log "Failed to update MKV metadata for $output_file" "31" true
    fi
}

# Main Function
# - This does everything

main() {
    check_dependencies
    parse_arguments "$@"
    validate_arguments
    setup_temp_directory
    generate_video_list
    corrupted_files=()
    ffprobe_errors=()
    filtered_convlist=()

    # Read the list of files to convert, capturing the exit status
    if ! mapfile -t convlist < <(filter_video_list); then
        echo "No files to convert after filtering."
        exit 0
    fi

    # If nothing, just exit out with a note
    if [[ ${#convlist[@]} -eq 0 ]]; then
        echo "No files to convert after filtering."
        exit 0
    fi

    log "Making sure the files are readable videos..." "34" true
    for file in "${convlist[@]}"; do
        # Try to probe the file
        if ! preformat_info=$(ffprobe -v error -select_streams v:0 \
            -show_entries stream=codec_name,width,height \
            -of default=noprint_wrappers=1:nokey=1 "$file" 2>&1); then
            # Store corrupt file and error - we'll show this later
            corrupted_files+=("$file")
            ffprobe_errors+=("$preformat_info")
        else
            # File is good, add to filtered list
            filtered_convlist+=("$file")
        fi
    done

    # Update the convlist to only include valid files
    convlist=("${filtered_convlist[@]}")
    total_files=${#convlist[@]}

    if [[ $total_files -eq 0 ]]; then
        echo "No valid files to process after removing corrupted files."
        exit 0
    fi

    # If we found corrupted files, display them
    if [[ ${#corrupted_files[@]} -gt 0 ]]; then
        echo
        echo "WARNING: The following files could not be processed due to corruption or invalid format:"
        echo "----------------------------------------------------------------"
        for ((i=0; i<${#corrupted_files[@]}; i++)); do
            echo "File: $(basename "${corrupted_files[i]}")"
            echo "Path: ${corrupted_files[i]}"
            echo "Error: ${ffprobe_errors[i]}"
            echo "----------------------------------------------------------------"
        done
        echo "Total corrupted files: ${#corrupted_files[@]}"
        echo "These files may need repair or replacement."
        echo
        echo "Proceeding with $total_files valid files"
        echo
    fi
    
    log "Files to convert:" "32" true
    for ((i=0; i<total_files; i++)); do
        file="${convlist[i]}"
        file_size=$(get_file_size "$file")
        preformat_info=$(get_video_info "$file")
        read -r codec_name width height <<< "$preformat_info"
        format=$(echo "$codec_name" | tr '[:lower:]' '[:upper:]')
        new_filename=$(transform_filename "$file")
        
        echo -e "\nProcessing file [$((i+1))/$total_files]"
        log "Original: $file" "32" true
        log "Size: $file_size, Format: $format" "32" true
        log "Will be renamed to: $new_filename" "32" true
        
        # Add debug logging for next file
        if ((i + 1 < total_files)); then
            next_file="${convlist[i+1]}"
            log "Next file will be: $next_file" "33" true
        else
            log "This is the last file" "33" true
        fi
    done

    if [[ $force != true ]]; then
        echo  # Add blank line before prompt
        log "About to ask for confirmation..." "34" true
        read -rp "Do you want to process these files? (y/n/1 for single file) " choice
        log "User choice was: $choice" "34" true
        case $choice in
            [Yy]*)
                log "User chose to process all files" "34" true
                ;;
            [Nn]*)
                log "User chose to exit" "34" true
                exit 
                ;;
            1)
                log "User chose single file selection" "34" true
                select file in "${convlist[@]}"; do
                    if [[ -n $file ]]; then
                        log "User selected file: $file" "34" true
                        encode_single_file "$file"
                    else
                        log "Invalid file selection" "31" true
                    fi
                    break
                done
                exit
                ;;
            *)
                log "Invalid choice. Exiting." "31" true
                echo "Invalid choice. Exiting."
                exit 1
                ;;
        esac
    else
        log "Force mode enabled, proceeding with all files" "34" true
    fi

    # Encode files sequentially with extra debugging
    for ((i=0; i<total_files; i++)); do
        # Display progress before each file
        display_encode_progress "$total_files" "$i"
        echo  
        
        current_file="${convlist[i]}"
        log "Starting to process file $((i+1))/$total_files: $current_file" "34" true
        
        if ((i + 1 < total_files)); then
            next_file="${convlist[i+1]}"
            log "Next file will be: $next_file" "33" true
        fi
        
        # Try to process the file and catch any errors
        if ! encode_single_file "${convlist[i]}"; then
            log "ERROR: Failed to process file: ${convlist[i]}" "31" true
            log "Last successful file was: ${convlist[i-1]}" "31" true
            log "Next file would have been: ${convlist[i+1]}" "31" true
            exit 1
        fi
        
        log "Successfully completed processing file: ${convlist[i]}" "32" true
        echo 
    done

    # Final progress update
    display_encode_progress "$total_files" "$total_files"
    echo  # Add newline
    log "All files processed." "32" true
    log "Total space saved: $(numfmt --to=iec --format="%.2f" "$total_space_saved")" "32" true
}

# Call Main
# - Since everything is a function, this just kicks off the script
main "$@"

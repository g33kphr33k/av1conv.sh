# av1conv.sh
## AV1 Conversion Script

## Description
This is a Bash script for Linux which automatically scans and converts video files to AV1 format (SVT-AV1) using ffmpeg. It includes features for handling audio tracks, subtitles, and maintaining quality while reducing file size. The script can also analyze source material and apply appropriate film grain synthesis.

## Installation
1. Place av1conv.sh in your path
2. Make it executable: `chmod +x av1conv.sh`
3. Run the script

## Usage
Usage: `av1conv.sh [options]`

Options:
- `-d, --dir DIR` = Specify the directory to scan (default: /mnt/movies/)
- `-1, --av1` = Force AV1 encoding for all files
- `-f, --force` = Process files without prompting
- `-F, --force-reencode` = Force re-encoding even if files are already in AV1 format
- `-v, --verbose` = Enable verbose output
- `-c, --crf VALUE` = Set CRF value (default: 38)
- `-p, --preset VALUE` = Set preset value (default: 8)
- `-g, --gop VALUE` = Set GOP size (default: 300)
- `-i, --ignore` = Ignore certain file types (e.g., CAM, WORKPRINT, TELESYNC)
- `-l, --lazy` = Run ffmpeg with 'nice' for lower CPU priority
- `-s, --size VALUE` = Minimum file size to process (e.g., 500M, 1G; default: 1G)
- `-r, --remove` = Remove the input file after successful encoding
- `-R, --resize-1080p` = Resize videos to 1080p if source resolution is higher

SVT-AV1 Specific Options:
- `--svt-tune VALUE` = Set tune value (default: 1)
- `--svt-overlays VALUE` = Enable/disable overlays (default: 1)
- `--svt-scd VALUE` = Enable/disable scene change detection (default: 1)
- `--svt-fast-decode VALUE` = Set fast decode value (default: 1)
- `--svt-lookahead VALUE` = Set lookahead frames (default: 120)
- `--svt-enable-qm VALUE` = Enable quantization matrices (default: 1)
- `--svt-qm-min VALUE` = Set min quantization matrix value (default: 0)
- `--svt-qm-max VALUE` = Set max quantization matrix value (default: 15)
- `--svt-tile-columns VALUE` = Set number of tile columns (default: 1)

Film Grain Options:
- `--svt-film-grain VALUE` = Set film grain level (0-50, default: 0)
  - 0: Disabled
  - 1-5: Light grain (good for digital content)
  - 6-10: Moderate grain (good for most film content)
  - 11-20: Strong grain (for heavy film grain content)
  - 21-50: Very strong grain (rarely needed)
- `--detect-grain` = Enable automatic film grain detection
  Note: Film grain synthesis significantly impacts encoding speed

Notes:
- All output files will be in Matroska (.mkv) format
- All audio is set to English as default track
- By default, only files over 1GB and not in AV1 or HEVC format are processed
- Target filename format: `original_name, AV1.mkv`
- The script automatically handles English subtitles (both text and PGS)
- Audio is automatically converted to Opus with appropriate bitrates based on channel count
- Automatic grain detection analyzes source material and applies appropriate grain settings
- Film grain synthesis can significantly increase encoding time

## Dependencies
The script requires the following tools:
- ffmpeg (with SVT-AV1 support)
- ffprobe
- nice
- numfmt
- find
- awk
- mkvpropedit
- jq
- GNU parallel

## Support
While this is primarily a personal tool, feel free to open issues for bugs or feature requests. For specific encoding questions, the FFmpeg community may be a better resource.

## Contributing
Contributions are welcome! Feel free to submit pull requests for improvements or bug fixes.

## Authors and acknowledgment
Created as a personal project, with thanks to the developers of FFmpeg, SVT-AV1, and all the required tools that make this possible.

## License
Free and open source. Use, modify, and distribute as you wish.

## Project status
Actively maintained and regularly updated.